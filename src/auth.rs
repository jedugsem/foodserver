use actix_web::{Error,web,dev::{ServiceRequest}};
use actix_web_httpauth::extractors::bearer::{BearerAuth,Config};
use actix_web_httpauth::extractors::AuthenticationError;
use crate::AppState;

pub async fn validator(req: ServiceRequest, credentials: BearerAuth) -> Result<ServiceRequest, Error> {
    let cred: Vec<&str> = credentials.token().split_whitespace().collect();
    let pass = cred[1];
    let name = cred[0];
    println!("wosj{}",name);
    let state = req.app_data::<web::Data<AppState>>().unwrap().clone();
    let mutex = state.token.lock().unwrap();
    let result = mutex.get(name);
    //let result = req.app_data::<web::Data<AppState>>().unwrap().token.lock().unwrap().clone().get(name).clone();
    if result.clone().is_some() {
        if pass == result.unwrap().0 {
            Ok(req)
        } else {
            let config = req
                .app_data::<Config>()
                .map(|data| data.clone())
                .unwrap_or_else(Default::default)
                .scope("urn:example:channel=HBO&urn:example:rating=G,PG-13");

            Err(AuthenticationError::from(config).into())
        }
    } else {
        let config = req
            .app_data::<Config>()
            .map(|data| data.clone())
            .unwrap_or_else(Default::default)
            .scope("urn:example:channel=HBO&urn:example:rating=G,PG-13");

        Err(AuthenticationError::from(config).into())
    }
}
