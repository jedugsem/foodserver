use serde::{Deserialize, Serialize};

#[derive(Clone, PartialEq, Deserialize, Debug, Serialize)]
pub struct Balance {
    pub balance: i32,
}
#[derive(Clone, PartialEq, Deserialize, Debug, Serialize)]
pub struct Trans {
    pub pro_id: i32,
    pub quantity: f32,
    pub unit: MeasureSend,
    pub end_price: i32,
}
#[derive(Clone, PartialEq, Deserialize, Debug, Serialize)]
pub struct BalanceList {
    pub vec: Vec<Trans>,
    pub balance: Balance,
}

#[derive(Clone, PartialEq, Deserialize, Debug, Serialize)]
pub enum MeasureSend {
    Grams,
    Kilo,
    Count,
    Liters,
    HunGram,
}
#[derive(Clone, PartialEq, Deserialize, Debug, Serialize)]
pub enum DataWrap {
    ProDb(ProDb),
    User(UserSend),
    Error,
}
#[derive(Clone, PartialEq, Deserialize, Debug, Serialize)]
pub struct ProDb {
    pub vec: Vec<Produkt>,
}
#[derive(Clone, PartialEq, Deserialize, Debug, Serialize)]
pub struct Produkt {
    pub id: i32,
    pub pro: String,
    pub image: Option<String>,
    pub desc: String,
    pub price: i32,
    pub stock: i32,
    pub mesure: MeasureSend,
}
#[derive(Clone, PartialEq, Deserialize, Debug, Serialize)]
pub struct UserSend {
    pub id: i32,
    pub username: String,
    //list : Option<Vec<u8>>,
    pub balance: i32,
}

#[derive(Clone, PartialEq, Deserialize, Debug, Serialize)]
pub struct Login {
    pub token: String,
    pub user: UserSend,
}
