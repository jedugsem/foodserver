use actix_cors::Cors;
//use tracing_subscriber::tracing
//use actix_web::Route;
use actix_web_httpauth::extractors::bearer::Config;
//use actix_web::HttpRequest;
//use rand::{distributions::Alphanumeric, Rng};
use actix_web::rt::spawn;
use std::sync::Arc;
use std::sync::Mutex;
use std::time::Duration;
use tokio::time;

//use serde::{Deserialize,Serialize};
//use actix_web::{get, post, web, App, HttpResponse, HttpServer, Responder,http,middleware::Logger};
use actix_web_httpauth::middleware::HttpAuthentication;
//use actix_files::Files as Fs;
use actix_web::{middleware::Logger, web, App, HttpServer};
use entity::sea_orm;
//use entity::pro;
//use entity::pro::Entity as Pro;
//use entity::pro::Measurment;
//use entity::sea_orm::QueryFilter;
//use entity::user;
//use entity::user::Entity as User;
//use listenfd::ListenFd;
//use entity::sea_orm::entity::*;

use entity::sea_orm::DatabaseConnection;

use migration::{Migrator, MigratorTrait};
use std::collections::HashMap;
use std::env;

mod auth;
mod handlers;
mod types;
use auth::validator;
//mod api::add_user;

#[derive(Debug, Clone)]
pub struct AppState {
    conn: DatabaseConnection,
    //config : Config,
//    tokens: Arc<Mutex<Vec<String>>>,
    token: Arc<Mutex<HashMap<String, (String, i32)>>>,
}


#[actix_web::main]
async fn main() -> std::io::Result<()> {
    //std::env::set_var("RUST_LOG", "()debug");
    //std::env::set_var("RUST_BACKTRACE", "1");
    //env_logger::init();

    //std::env::set_var("RUST_LOG", "debug");

    // get env vars
    //dotenv::dotenv().ok();

    //let _db_url = env::var("DATABASE_URL").expect("DATABASE_URL is not set in .env file");

    // create post table if not exists
    let url = "postgres://postgres@localhost/fooddata".to_string();
    let conn = sea_orm::Database::connect(&url).await.unwrap();
    let mut map = HashMap::new();
    map.insert("manu".to_string(), ("grep".to_string(), 0));
    Migrator::up(&conn, None).await.unwrap();
    let token = Arc::new(Mutex::new(map));
    let state = AppState {
        conn,
//        tokens,
        token: token.clone(),
    };
    //let mut listenfd = ListenFd::from_env();

    HttpServer::new(move || {
        //let auth = HttpAuthentication::bearer(validator);
        let logger = Logger::default();

    //tracing_subscriber::fmt().with_max_level(tracing::Level::DEBUG).with_test_writer().init();
        let cors = Cors::permissive(); //default() .supports_credentials()
                                       //.disable_preflight() .allow_any_origin() .allow_any_header();

    spawn(async move {
    let mut interval = time::interval(Duration::from_secs(10));
    loop {
        interval.tick().await;
        println!("every ten");
    }
});
        App::new()
            .app_data(web::Data::new(state.clone()))
            .app_data(
                Config::default()
                    .realm("Restricted area")
                    .scope("email photo"),
            )
            .wrap(logger)
            .wrap(cors)
            .service(
                web::scope("/auth")
                    //.wrap(auth)
    .route("/setup", web::post().to(handlers::setup::setup_db))
                    .route("/balance", web::get().to(handlers::balance::get_balance))
//    .route("/adduser",web::get().to(handlers::user::post_user))
            )
            // .service(api::storage::list)
            .configure(init)
    })
    //.bind(("127.0.0.1", 443))?
    .bind(("127.0.0.1", 5003))?
    .run()
    .await
}

pub fn init(cfg: &mut web::ServiceConfig) {
    cfg.route("/data/db", web::get().to(handlers::db::get_produckt_db));
    cfg.route("/data/user", web::get().to(handlers::user::get_user));
    cfg.route("/data/buy", web::post().to(handlers::buy::post_buy));
    cfg.route(
        "/data/submit",
        web::post().to(handlers::submit::post_submit),
    );
    cfg.route(
        "/data/managment",
        web::post().to(handlers::test::post_managment),
    );
    //cfg.route( "/data/balance", web::get().to(handlers::balance::get_balance),);
    cfg.route("/data/adduser", web::post().to(handlers::user::post_user));
    cfg.route("/setup", web::post().to(handlers::setup::setup_db));
    cfg.route("/login", web::post().to(handlers::login::login));
}
