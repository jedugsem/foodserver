use actix_web::{web, HttpResponse, Responder};
use entity::pro;
use entity::pro::Entity as Pro;
use entity::pro::Measurment;
use entity::sea_orm::EntityTrait;
use entity::sea_orm::Set;

use serde::Deserialize;

//use crate::types::*;
use crate::AppState;

#[derive(Debug, Deserialize)]
pub struct SubmitParams {
    name: String,
    image: String,
    price: String,
    stock: String,
    desc: String,
    unit: String,
}
pub async fn post_submit(
    req: web::Query<SubmitParams>,
    data: web::Data<AppState>,
) -> impl Responder {
    let _conn = &data.conn;
    println!("{:?}", &req);
    let price = req.price.parse::<i32>();
    let stock = req.stock.parse::<i32>();
    let image = if req.image.len() > 3 {
        Some(req.image.clone())
    } else {
        None
    };
    let unit = match req.unit.as_str() {
        "count" => Some(Measurment::Count),
        "hgrams" => Some(Measurment::HunGram),
        "liters" => Some(Measurment::Liters),
        "grams" => Some(Measurment::Grams),
        "kilos" => Some(Measurment::Kilo),
        _ => None,
    };
    if unit.is_some() && price.is_ok() && stock.is_ok() {
        let produkt = pro::ActiveModel {
            pro: Set(req.name.clone()),
            image: Set(image),
            desc: Set(req.desc.clone()),
            price: Set(price.unwrap()),
            stock: Set(stock.unwrap()),
            measure: Set(unit.unwrap()),
            ..Default::default()
        };
        Pro::insert(produkt)
            .exec(_conn)
            .await
            .expect("insert_failed");

        HttpResponse::Ok().body("Ok")
    } else {
        HttpResponse::Ok().body("Parse- Error")
    }
}
