use actix_web::{web, HttpResponse, Responder};
use sha2::{Digest, Sha256};

//use actix_web_httpauth::extractors::bearer::BearerAuth;

use entity::pro::Measurment;

use entity::pro;
use entity::pro::Entity as Pro;
use entity::sea_orm::DeleteResult;
use entity::sea_orm::EntityTrait;
use entity::sea_orm::Set;
use entity::sell::Entity as Sell;
use entity::user;
use entity::user::Entity as User;

use serde::Deserialize;

//use crate::types::*;
use crate::AppState;

#[derive(Deserialize)]
pub struct SetupParams {
    com: String,
}

//#[get("/start")]
pub async fn setup_db(
    //token: BearerAuth,
    req: web::Query<SetupParams>,
    data: web::Data<AppState>,
) -> impl Responder {
    let conn = &data.conn;
    //println!("{}",id);

    //println!("token:{}",token.token());
    //println!("yppppp{}\n\n",req.com);
    if req.com == "fill" {
        let apple = pro::ActiveModel {
            pro: Set("Apple".to_owned()),
            image: Set(Some("url".to_owned())),
            desc: Set("apple to eat, price per 1kg".to_owned()),
            price: Set(150),
            stock: Set(3),
            measure: Set(Measurment::Kilo),
            ..Default::default()
        };
        let bier = pro::ActiveModel {
            pro: Set("Bier".to_owned()),
            image: Set(Some("url".to_owned())),
            desc: Set("leckeres bier".to_owned()),
            price: Set(100),
            stock: Set(27),
            measure: Set(Measurment::Count),
            ..Default::default()
        };
        let sugar = pro::ActiveModel {
            pro: Set("Brown Sugar".to_owned()),
            image: Set(Some("url".to_owned())),
            desc: Set(
                "Rohrzucker, price per 1kg ajsjgdhalskjf;a ajkhsdlgj adshgfasjdk jasjgdfjavca affh"
                    .to_owned(),
            ),
            price: Set(280),
            stock: Set(9),
            measure: Set(Measurment::Kilo),
            ..Default::default()
        };

        let salt = "matz@gmc.yo".to_string();
        let mut hasher1 = Sha256::new();
        let mut hasher2 = Sha256::new();
        hasher1.update(format!("{}{}", salt, "never"));
        hasher2.update(format!("{}{}", salt, "cold"));
        let password1: Vec<u8> = hasher1.finalize().to_vec();
        let password2: Vec<u8> = hasher2.finalize().to_vec();

        let test_user = user::ActiveModel {
            username: Set("Julia".to_owned()),
            password: Set(password1),
            balance: Set(10000),
            salt : Set(salt.clone()),
            ..Default::default()
        };
        let test_user1 = user::ActiveModel {
            username: Set("manu".to_owned()),
            password: Set(password2),
            balance: Set(5000),
            salt : Set(salt),
            ..Default::default()
        };

        Pro::insert_many(vec![apple, sugar, bier])
            .exec(conn)
            .await
            .expect("failed insert");
        User::insert_many(vec![test_user, test_user1])
            .exec(conn)
            .await
            .expect("failed insert");

        HttpResponse::Ok().body("2")
    } else if req.com == "checkdb" {
        let pro = Pro::find().one(conn).await;
        let user = User::find().one(conn).await;
        println!("{:?}", pro);
        println!("{:?}", user);

        if pro.is_ok() && user.is_ok() {
            if pro.unwrap().is_some() && user.unwrap().is_some() {
                HttpResponse::Ok().body("1")
            } else {
                HttpResponse::Ok().body("0")
            }
        } else {
            HttpResponse::Ok().body("0")
        }
    } else if req.com == "delete" {
        println!("yeaah\n\n");
        let _res: DeleteResult = Sell::delete_many().exec(conn).await.unwrap();
        let _res: DeleteResult = Pro::delete_many().exec(conn).await.unwrap();
        let _res: DeleteResult = User::delete_many().exec(conn).await.unwrap();
        HttpResponse::Ok().body("0")
    } else {
        HttpResponse::Ok().body(req.com.clone())
    }
}
