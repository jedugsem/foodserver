use actix_web::{web, HttpResponse, Responder};

//use actix_web_httpauth::extractors::bearer::BearerAuth;

use entity::pro;
use entity::pro::Entity as Pro;
use entity::sea_orm::ActiveModelTrait;
use entity::sea_orm::EntityTrait;
use entity::sea_orm::Set;
use entity::sell;
use entity::sell::Entity as Sell;
use entity::user;
use entity::user::Entity as User;

use serde::Deserialize;

use crate::AppState;

#[derive(Debug, Deserialize)]
pub struct BuyParams {
    q: f32,
    pro: i32,
    user: i32,
}

pub async fn post_buy(req: web::Query<BuyParams>, data: web::Data<AppState>) -> impl Responder {
    let conn = &data.conn;

    //let sugar_data: Vec<pro::Model> = Pro::find()
    //        .filter(pro::Column::Pro.contains("Sugar"))
    //        .all(conn)
    //        .await.expect("crash");

    //    let test_user_data: Vec<user::Model> = User::find()
    //        .filter(user::Column::Username.contains("Julia"))
    //       .all(conn)
    //       .await.expect("crash");

    //println!("{}\n\n\n\n\n\n{}",sugar_data.len(),test_user_data.len());
    //if sugar_data.len() == 1 && test_user_data.len() == 1{

    let pro: pro::Model = Pro::find_by_id(req.pro).one(conn).await.unwrap().unwrap();
    println!("{:?}", pro.measure);
    let end_price = (pro.price as f32 * req.q) as i32;

    let user: Option<user::Model> = User::find_by_id(req.user)
        .one(conn)
        .await
        .expect("dfailed to get user");

    let balance = user.clone().unwrap().balance;
    // Into ActiveModel
    let mut user: user::ActiveModel = user.unwrap().into();

    // Update name attribute
    user.balance = Set(balance - end_price);

    // Update corresponding row in database using primary key value
    let _user: user::Model = user.update(conn).await.expect("failed Blance update");

    let sell = sell::ActiveModel {
        pro_id: Set(req.pro),
        user_id: Set(req.user),
        unit: Set(pro.measure),
        quantity: Set(req.q),
        end_price: Set(end_price),
        ..Default::default()
    };
    Sell::insert(sell).exec(conn).await.expect("failed sell");
    println!("test\n\n\n\n");
    //}

    HttpResponse::Ok().body("Hey there!")
}
