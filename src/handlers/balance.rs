//use sha2::{Sha256,Digest};
use actix_web::{web, HttpResponse, Responder};

use entity::pro::Measurment;

use entity::sea_orm::ColumnTrait;
use entity::sea_orm::EntityTrait;
use entity::sea_orm::QueryFilter;
use entity::sell;
use entity::sell::Entity as Sell;
use entity::user;
use entity::user::Entity as User;
use serde::Deserialize;

use crate::types::*;
use crate::AppState;

#[derive(Debug, Deserialize)]
pub struct BalanceParams {
    id: String,
    history: String,
}
pub async fn get_balance(
    req: web::Query<BalanceParams>,
    data: web::Data<AppState>,
) -> impl Responder {
    ////    println!("{:?}", auth.token());
    //    if auth.token() == data.tokens.as_ref().lock().unwrap()[0] {
    //        println!("hey it works");
    //    }

    //   println!("{:?}", data.token.as_ref().lock().unwrap().get("manu"));
    println!("balance");
    let _conn = &data.conn;
    let history = req.history.parse::<i32>();
    let id = req.id.parse::<i32>();

    if id.is_ok() && history.is_ok() {
        let history = history.unwrap();
        let id = id.unwrap();
        let user: user::Model = User::find_by_id(id).one(_conn).await.unwrap().unwrap();
        let sells: Vec<sell::Model> = Sell::find()
            .filter(sell::Column::UserId.eq(id))
            .all(_conn)
            .await
            .expect("fail");
        let vector: Vec<Trans> = sells
            .iter()
            .map(|sell| Trans {
                pro_id: sell.pro_id,
                quantity: sell.quantity,
                unit: match sell.unit {
                    Measurment::Grams => MeasureSend::Grams,
                    Measurment::Kilo => MeasureSend::Kilo,
                    Measurment::Liters => MeasureSend::Liters,
                    Measurment::Count => MeasureSend::Count,
                    Measurment::HunGram => MeasureSend::HunGram,
                },
                end_price: sell.end_price,
            })
            .collect();

        //let mut rich = user.balance;
        //for i in vector.iter() {
        //    rich -= i.end_price;
        //}
        if history == 0 {
            //let sells = Sell::find()

            let data = BalanceList {
                vec: vector,
                balance: Balance {
                    balance: user.balance,
                },
            };

            let bindata: Vec<u8> = bincode::serialize(&Some(data)).unwrap();

            HttpResponse::Ok().body(bindata)
        } else if history == 100 {
            let data = BalanceList {
                vec: vec![],
                balance: Balance { balance: 30 },
            };
            let bindata: Vec<u8> = bincode::serialize(&Some(data)).unwrap();

            HttpResponse::Ok().body(bindata)
        } else {
            let data = BalanceList {
                vec: vec![],
                balance: Balance { balance: 50 },
            };
            let bindata: Vec<u8> = bincode::serialize(&Some(data)).unwrap();

            HttpResponse::Ok().body(bindata)
        }
    } else {
        HttpResponse::Ok().body("Parse- Error")
    }
}
