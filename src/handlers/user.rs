use actix_web::{web, HttpResponse, Responder};
use sha2::{Sha256,Digest};
use entity::sea_orm::ColumnTrait;
use entity::sea_orm::EntityTrait;
use entity::sea_orm::QueryFilter;
use entity::sea_orm::Set;
use entity::user;
use entity::user::Entity as User;

use serde::Deserialize;

use crate::types::*;
use crate::AppState;

#[derive(Deserialize)]
pub struct UserParams {
    name: String,
}

pub async fn get_user(req: web::Query<UserParams>, data: web::Data<AppState>) -> impl Responder {
    let conn = &data.conn;
    let user: Vec<user::Model> = User::find()
        .filter(user::Column::Username.contains(&req.name))
        .all(conn)
        .await
        .unwrap();
    if user.len() == 1 {
        let data = UserSend {
            id: user[0].id,
            username: user[0].username.clone(),
            balance: user[0].balance,
        };
        let bindata: Vec<u8> = bincode::serialize(&Some(data)).unwrap();
        //ctx.binary(bindata);

        HttpResponse::Ok().body(bindata)
    } else {
        let none: Option<UserSend> = None;
        let bindata: Vec<u8> = bincode::serialize(&none).unwrap();
        HttpResponse::Ok().body(bindata)
    }
}

//#[derive(Debug, Deserialize)]
//pub struct UserAddParams {
//    name: String,
//    balance: String,
//}

pub async fn post_user(
//    req: web::Query<UserAddParams>,
    data: web::Data<AppState>,
    body : String,
) -> impl Responder {
    let dat : Vec<&str> = body.split_whitespace().collect();
    let user = dat[0];
    let balance = dat[1];
    let password = dat[2];
    let salt = dat[3];
    println!("{:?}",dat);
    let conn = &data.conn;
    let balance = balance.parse::<i32>();
    if balance.is_ok() && user.len() > 0 {
        let balance = balance.unwrap();

        
         let mut hasher1 = Sha256::new();
         hasher1.update(format!("{}{}", salt, password));
         let password1: Vec<u8> = hasher1.finalize().to_vec();

        let user = user::ActiveModel {
            username: Set(user.to_string().clone()),
            balance: Set(balance),
            password: Set(password1),
            salt: Set(salt.to_string()),
            ..Default::default()
        };
        User::insert(user).exec(conn).await.expect("error");
        HttpResponse::Ok().body("Ok")
    } else {
        HttpResponse::Ok().body("Parse- Error")
    }

//    let balance = req.balance.parse::<i32>();
//    if balance.is_ok() && req.name.len() > 0 {
//        let balance = balance.unwrap();
//        let user = user::ActiveModel {
//            username: Set(req.name.clone()),
//            balance: Set(balance),
//            password: Set(vec![]),
//            ..Default::default()
//        };
//        User::insert(user).exec(conn).await.expect("error");
//        HttpResponse::Ok().body("Ok")
//    } else {
//        HttpResponse::Ok().body("Parse- Error")
 //   }
}
