use actix_web::{web, HttpResponse, Responder};

//use actix_web_httpauth::extractors::bearer::BearerAuth;

use entity::pro::Measurment;

use entity::pro;
use entity::pro::Entity as Pro;

use entity::sea_orm::EntityTrait;
//use entity::sea_orm::QueryFilter

use crate::types::*;
use crate::AppState;

pub async fn get_produckt_db(data: web::Data<AppState>) -> impl Responder {
    let conn = &data.conn;

    let model_vec: Vec<pro::Model> = Pro::find().all(conn).await.unwrap();
    let vec: Vec<Produkt> = model_vec
        .into_iter()
        .map(|pro| Produkt {
            id: pro.id,
            pro: pro.pro,
            image: pro.image,
            desc: pro.desc,
            price: pro.price,
            stock: pro.stock,
            mesure: match pro.measure {
                Measurment::Grams => MeasureSend::Grams,
                Measurment::Kilo => MeasureSend::Kilo,
                Measurment::Liters => MeasureSend::Liters,
                Measurment::Count => MeasureSend::Count,
                Measurment::HunGram => MeasureSend::HunGram,
            },
        })
        .collect();
    let data: DataWrap = DataWrap::ProDb(ProDb { vec });
    let bindata: Vec<u8> = bincode::serialize(&Some(data)).unwrap();
    //ctx.binary(bindata);

    HttpResponse::Ok().body(bindata)
}
