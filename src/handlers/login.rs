use actix_web::{web, HttpResponse, Responder};
use rand::{distributions::Alphanumeric, Rng};
use sha2::{Digest, Sha256};

use entity::sea_orm::ColumnTrait;
use entity::sea_orm::EntityTrait;
use entity::sea_orm::QueryFilter;
use entity::user;
use entity::user::Entity as User;

use crate::types::*;
use crate::AppState;

//#[derive(Debug, Deserialize)]
//struct Login {
//    name : String,
//    password : String,
//}

pub async fn login(body: String, data: web::Data<AppState>) -> impl Responder {
    println!("test{:?}", body);
    let bod: Vec<&str> = body.split_whitespace().collect();
    println!("user:{},\npasswor:{},", bod[0], bod[1]);
    let pass = bod[1];
    let user = bod[0];

    let conn = &data.conn;

    let salt = "matz@gmc.yo".to_string();

    let users: Vec<user::Model> = User::find()
        .filter(user::Column::Username.contains(user))
        .all(conn)
        .await
        .unwrap();
    let salt = users[0].salt.clone();
    let mut hasher_password = Sha256::new();
    hasher_password.update(format!("{}{}", salt, pass).into_bytes());
    let pass_hash: Vec<u8> = hasher_password.finalize().to_vec();
    println!("{}", users.len());
    if users.len() == 1 {
        println!("wesh");
        if users[0].password == pass_hash {
            let s: String = rand::thread_rng()
                .sample_iter(&Alphanumeric)
                .take(20)
                .map(char::from)
                .collect();
            println!("s:{}", s);
            if let Ok(mut tokens) = data.token.lock() {
                let i = user;
                let user = &users.clone()[0];
                tokens.insert(user.username.clone(), (s.clone(), user.id));
                let data = Login {
                    user: UserSend {
                        id: user.id,
                        username: user.username.clone(),
                        balance: user.balance,
                    },
                    token: format!("{} {}", i, s),
                };
                let bindata: Vec<u8> = bincode::serialize(&data).unwrap();
                println!("succes");
                HttpResponse::Ok().body(bindata)
            } else {
                HttpResponse::Ok().body("tokens broken")
            }
        } else {
            HttpResponse::Ok().body("password false")
        }
    } else {
        HttpResponse::Ok().body("username false")
    }
}
