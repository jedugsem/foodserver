use sea_orm_migration::prelude::*;
use entity::pro;
use entity::pro::Entity as Pro;
use entity::sell;
use entity::sell::Entity as Sell;
use entity::user;
use entity::user::Entity as User;

pub struct Migration;

impl MigrationName for Migration {
    fn name(&self) -> &str {
        "m20220101_000001_create_table"
    }
}

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
                manager
            .create_table(
                Table::create()
                    .table(Sell)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(sell::Column::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(sell::Column::ProId).integer().not_null())
                    .col(ColumnDef::new(sell::Column::UserId).integer().not_null())
                    .col(ColumnDef::new(sell::Column::Unit).string().not_null())
                    .col(ColumnDef::new(sell::Column::Quantity).float().not_null())
                    .col(ColumnDef::new(sell::Column::EndPrice).integer().not_null())
                    .to_owned(),
            )
            .await?;
        manager
            .create_table(
                Table::create()
                    .table(User)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(user::Column::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(user::Column::Username).string().not_null())
                    .col(ColumnDef::new(user::Column::Salt).string().not_null())
                    .col(ColumnDef::new(user::Column::Password).binary())
                    .col(ColumnDef::new(user::Column::Balance).integer().not_null())
                    .to_owned(),
            )
            .await?;

        manager
            .create_table(
                Table::create()
                    .table(Pro)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(pro::Column::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(pro::Column::Pro).string().not_null())
                    .col(ColumnDef::new(pro::Column::Image).string())
                    .col(ColumnDef::new(pro::Column::Desc).string().not_null())
                    .col(ColumnDef::new(pro::Column::Stock).integer().not_null())
                    .col(ColumnDef::new(pro::Column::Measure).string().not_null())
                    .col(ColumnDef::new(pro::Column::Price).integer().not_null())
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(Sell).to_owned())
            .await?;

        manager
            .drop_table(Table::drop().table(User).to_owned())
            .await?;
        manager
            .drop_table(Table::drop().table(Pro).to_owned())
            .await
    }
}
