use sea_orm::entity::prelude::*;
use serde::{Deserialize, Serialize};

use crate::pro::Measurment;

#[derive(Clone, Debug, PartialEq, DeriveEntityModel, Deserialize, Serialize)]
#[sea_orm(table_name = "sell")]
pub struct Model {
    #[sea_orm(primary_key)]
    #[serde(skip_deserializing)]
    pub id: i32,
    pub pro_id: i32,
    pub unit: Measurment,
    pub user_id: i32,
    pub quantity: f32,
    pub end_price: i32,
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {}

impl ActiveModelBehavior for ActiveModel {}

//#[derive(Debug, Clone, PartialEq, EnumIter, DeriveActiveEnum, Deserialize, Serialize)]
//  #[sea_orm(rs_type = "String", db_type = "String(Some(1))")]
//  pub enum Measurment {
//     #[sea_orm(string_value = "G")]
//     Grams,
//    #[sea_orm(string_value = "K")]
//    Kilo,
//     #[sea_orm(string_value = "C")]
//     Count,
//     #[sea_orm(string_value = "L")]
//     Liters,
//     #[sea_orm(string_value = "H")]
//    HunGram,
//}
